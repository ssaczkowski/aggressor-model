import logging
import pickle
import warnings

import self as self
from keras.layers import Dropout

with warnings.catch_warnings():
    warnings.filterwarnings("ignore", category=FutureWarning)
    import pandas as pd
    from sklearn.feature_extraction.text import CountVectorizer
    from sklearn.metrics import confusion_matrix
    from sklearn.metrics import accuracy_score
    from sklearn.metrics import precision_score
    from sklearn.metrics import recall_score
    from sklearn.metrics import f1_score
    from sklearn.utils import resample
    from keras import layers,models
    from keras.wrappers.scikit_learn import KerasClassifier
    from sklearn.model_selection import cross_val_score
    from sklearn.model_selection import GridSearchCV
    import matplotlib.pyplot as plt
    from sklearn.externals import joblib

# Logging

logging.basicConfig(filename="fit_model",
                            filemode='a',
                            format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                            datefmt='%H:%M:%S',
                            level=logging.DEBUG)
def log(data):
    logging.info(data)
    self.logger = logging.getLogger('fit_model')

# DATASET
filepath_dict = {'dataset': 'dataset.csv',
                 'dataset_excluded': 'dataset_excluded.csv'}

df_list = []
for source, filepath in filepath_dict.items():
    df = pd.read_csv(filepath, names=['sentence', 'label'], sep=',')
    df['source'] = source
    df_list.append(df)
df = pd.concat(df_list)

print(df.describe)
print((df.groupby('label')).size())
print(df.shape)

sentences_list = []
for sentence in df.values:
     sentences_list.append(sentence[0])


df_dataset = df[df['source'] == 'dataset']

sentences = df_dataset['sentence'].values
y = df_dataset['label'].values

# Graphic
def plot_history(history):
    acc = history.history['acc']
    #val_acc = history.history['val_accuracy']
    loss = history.history['loss']
    #val_loss = history.history['val_loss']
    x = range(1, len(acc) + 1)
    plt.figure(figsize=(12, 5))
    plt.subplot(1, 2, 1)
    plt.plot(x, acc, 'b', label='Entrenamiento prec')
    #plt.plot(x, val_acc, 'r', label='Validacion prec')
    plt.title('Precision Entrenamiento y validacion')
    plt.legend()
    plt.subplot(1, 2, 2)
    plt.plot(x, loss, 'b', label='Perdida Entrenamiento')
    #plt.plot(x, val_loss, 'r', label='Perdida Validacion')
    plt.title('Perdida Entrenamiento y validacion')
    plt.legend()
    plt.show()


def evaluate_model(sentences_train, y_train, sentences_test, y_test):

    vectorizer = CountVectorizer(ngram_range=(1,3))
    vectorizer.fit(sentences_train)

    # Save vectorizer.vocabulary_
    pickle.dump(vectorizer.vocabulary_, open("dictionary.pkl", "wb"), protocol=2)

    print("DICTIONARY JSON :" + str(vectorizer.vocabulary_))
    print("#DICTIONARY :" + str(vectorizer.vocabulary_.__sizeof__()))

    X_train = vectorizer.transform(sentences_train)
    X_test = vectorizer.transform(sentences_test)

    input_dim = X_train.shape[1]# Number of features
    print("#FEATURES : " + str(input_dim))
    def nn_model(optimizer='adamax',activationC0='linear', activationC1='softmax',activationC2='tanh',use_biasC0=True,use_biasC1=True,use_biasC2=True,dropout=0.0,loss='logcosh'):
        model = models.Sequential()

        model.add(layers.Dense(100, input_dim=input_dim, use_bias=use_biasC0, activation=activationC0))

        model.add(Dropout(0.1))

        model.add(layers.Dense(50, activation=activationC1, use_bias=use_biasC1))

        model.add(Dropout(0.1))

        model.add(layers.Dense(20, activation=activationC2, use_bias=use_biasC2))

        model.add(Dropout(0.1))

        model.add(layers.Dense(10, activation=activationC2, use_bias=use_biasC2))

        model.add(Dropout(0.0))

        model.add(layers.Dense(1, activation='sigmoid'))

        model.compile(loss=loss,
                    optimizer=optimizer,
                    metrics=['acc'])
        return model

    # CROSS VALIDATION & Fine Tuning

    # parameters = {
    #                 'loss': (
    #                 'binary_crossentropy', 'mean_absolute_error', 'mean_absolute_percentage_error',
    #                 'logcosh', 'huber_loss',
    #                 'kullback_leibler_divergence'),
    #                 'dropout': (0.4, 0.1,0.5,0.0),
    #                 'activationC0': ('softmax', 'sigmoid', 'exponential', 'selu', 'softplus','relu','linear'),
    #                 'activationC1': ('softmax', 'elu', 'selu', 'softplus'),
    #                 'activationC2': ( 'tanh', 'elu', 'selu', 'linear'),
    #                 'optimizer': ('adam', 'sgd', 'rmsprop', 'adamax', 'nadam'),
    #                 'use_biasC0': (True,False),
    #                 'use_biasC1': (True, False),
    #                 'use_biasC2': (True, False)
    #             }

    # parameters = {
    #
    #     'dropout': (0.4,0.0),
    #     'activationC2': ( 'tanh','elu')
    # }


    estimator = KerasClassifier(build_fn=nn_model, epochs=50, batch_size=60)#batch_size 60 o 20.

    #grid_search = GridSearchCV(estimator=estimator, param_grid=parameters, scoring='accuracy', cv=3)

    #grid_result = grid_search.fit(X_train,y_train)

    #sumarizeResults(grid_result)

    #model = grid_result #.best_estimator_


    history = estimator.fit(X_train, y_train)

    # accuracies = cross_val_score(estimator, X_train, y_train, cv=3, n_jobs=-1)
    # mean_acc = accuracies.mean()
    # print("ACC CROSS_VALIDATION = " + str(mean_acc))

    #model.summary()

    # history = estimator.fit(X_train, y_train,
    #                     epochs=50,
    #                     verbose=2,
    #                     validation_data=(X_test, y_test),
    #                     batch_size=20)

    y_pred = predictions(sentences_train,sentences_test, estimator)

    metrics(y_test,y_pred)

    return estimator.model, accuracy_score(y_test, y_pred),history


def sumarizeResults(grid_result):
    print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
    log(grid_result.best_score_)
    log(grid_result.best_params_)
    means = grid_result.cv_results_['mean_test_score']
    stds = grid_result.cv_results_['std_test_score']
    params = grid_result.cv_results_['params']
    for mean, stdev, param in zip(means, stds, params):
        print("%f (%f) with: %r" % (mean, stdev, param))


def predictions(sentences_train , sentences_test, model):
    vectorizer = CountVectorizer(ngram_range=(1,3))
    vectorizer.fit(sentences_train)

    y_pred = []
    for s in sentences_test:
        aux = []
        aux.append(s)
        X = vectorizer.transform(aux)
        prediction = model.predict(X)
        if prediction > 0.5:
            y_pred.append(1)
        else:
            y_pred.append(0)

    return y_pred

# Metrics
def metrics(y_test,y_pred):
    print(" 1 : Confusion Matrix")
    matriz = confusion_matrix(y_test, y_pred)
    print(matriz)
    print(" 2 : Accuracy")
    exactitud = accuracy_score(y_test, y_pred)
    print(exactitud)
    print(" 3 : Precision")
    precision = precision_score(y_test, y_pred)
    print(precision)
    print(" 4 : Sensitivity")
    sensibilidad = recall_score(y_test, y_pred)
    print(sensibilidad)
    print(" 5 : F1 Score")
    puntaje = f1_score(y_test, y_pred)
    print(puntaje)


def saveModel(model):
    model.save("aggressormodel.h5")



bestPrecision = 0.0
bestModel = models.Sequential()
sentencesTrainXForBestModel = [] #for the dictionary of prediction's method
sentencesTrainYForBestModel = []

n_splits = 1
scores, members = list(), list()
for iteracion in range(n_splits):
    print(f"---------------------------------------------------------Iteracion:{iteracion}")

    ix = [i for i in range(len(sentences))]
    train_ix = resample(ix, replace=True, n_samples=1000)
    test_ix = [x for x in ix if x not in train_ix]

    trainX, trainY = sentences[train_ix], y[train_ix]
    testX, testy = sentences[test_ix], y[test_ix]

    model, test_acc , history = evaluate_model(trainX, trainY, testX, testy)


   # plot_history(history)

    if(bestPrecision < test_acc):
         bestPrecision = test_acc
         bestModel = model
         sentencesTrainXForBestModel = trainX
         sentencesTrainYForBestModel = trainY
    print('>%.3f' % test_acc)
    scores.append(test_acc)
    members.append(model)
    saveModel(bestModel)

print(scores)
print(members)

bestPrecisionMsg = "\nTHE BEST MODEL: \nPrecision = " + str(bestPrecision)
print(bestPrecisionMsg)
log(bestPrecisionMsg)

# Evaluate dataset_excluded

df_excluded = df[df['source'] == 'dataset_excluded']
sentences_datasetExcluded = df_excluded['sentence'].values
y_datasetExcluded = df_excluded['label'].values

y_pred_datasetExcluded = predictions(sentencesTrainXForBestModel, sentences_datasetExcluded, bestModel)

print("\n METRICS FOR DATASET EXCLUDED: \n")

mistakes = 0
for i in range(len(y_pred_datasetExcluded)):
    if(y_pred_datasetExcluded[i] == 1):
        mistakes += 1

print(y_pred_datasetExcluded)

mistakePercentage = (mistakes * 100) / len(y_pred_datasetExcluded)
successesPercentage = 100.0 - mistakePercentage

print("Mistake Percentage = " + str(mistakePercentage))
print("Successes Percentage = " + str(successesPercentage))

print("Mistakes = " + str(mistakes))
print("Successes = " + str(len(y_pred_datasetExcluded) - mistakes))




import pickle
import warnings
import sys

from keras.engine.saving import load_model

with warnings.catch_warnings():
    warnings.filterwarnings("ignore", category=FutureWarning)
    from sklearn.feature_extraction.text import CountVectorizer


# Save data

file = open('predictions', 'a+')

# Load model
model = load_model('aggressormodel.h5')

# Load dictionary

vectorizer = CountVectorizer(decode_error="replace",vocabulary=pickle.load(open("dictionary.pkl", "rb")))

tweet = sys.argv[1]
tweet_array = []

tweet_array.append(tweet)

X = vectorizer.transform(tweet_array)

prediction = model.predict(X)

if prediction > 0.5:
    file.write(tweet + "," + str(1))
    print(1)
else:
    file.write(tweet + "," + str(0))
    print(0)

file.write("\r\n")
file.close()


sys.stdout.flush()
sys.exit(0)